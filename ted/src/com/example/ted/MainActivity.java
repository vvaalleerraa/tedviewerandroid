package com.example.ted;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;



import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSReader;
import org.mcsoxford.rss.RSSReaderException;
import org.xml.sax.SAXException;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	String[] month = {
			"January", "February", "March", "April",
			"May", "June", "July", "August",
			"September", "October", "November", "December"
	};
	class tedRow {
		String title;
		Bitmap thumb;
		String videoUrl;
		tedRow(Bitmap preLoadSrcBitmap, String inTitle,
				String videoLink) { 
			thumb = preLoadSrcBitmap; 
			title = inTitle;
			videoUrl = videoLink;
		}
	}
	ArrayList<tedRow> tedList;
	ArrayAdapter adapter; 
	
	public class backgroundLoadListView extends
	AsyncTask<Void, Void, Void> {

		@Override
		protected void onPostExecute(Void result) {
			//We moved it to PreEx: setListAdapter(new MyCustomAdapter(MainActivity.this, R.layout.row, tedList));
			Toast.makeText(MainActivity.this,
					"onPostExecute \n: setListAdapter after bitmap preloaded",
					Toast.LENGTH_LONG).show();
			setListAdapter(new MyCustomAdapter(MainActivity.this, R.layout.row, tedList));
			((ArrayAdapter<tedRow>) getListAdapter()).notifyDataSetChanged();
		}

		@Override
		protected void onPreExecute() {
			Toast.makeText(MainActivity.this,
					"onPreExecute \n: preload bitmap in AsyncTask",
					Toast.LENGTH_LONG).show();
			tedList.clear();
		}
		
		protected void onProgressUpdate(Integer... progress) {

		}

		@Override
		protected Void doInBackground(Void... params) {
		
			try {
				//url = new URL("http://www.ted.com/themes/rss/id/6");
				
				RSSReader reader = new RSSReader(); 
				String uri = "http://www.ted.com/themes/rss/id/6"; 
				RSSFeed feed = reader.load(uri);

				List<RSSItem> rssItems = feed.getItems();
				int maxItems=20;
				int nit=maxItems;
				
				bm=BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);;
				
				for(RSSItem rssItem : rssItems) {
					Log.i("RSS Reader", rssItem.getTitle());
					//Log.i("RSS Reader", rssItem.getLink().toString());
					Log.i("RSS Reader", rssItem.getEnclosure().getUrl().toString() );
					Log.i("RSS Reader", rssItem.getThumbnails().get(0).getUrl().toString());
					
					//preLoadSrcBitmap(rssItem.getThumbnails().get(0).getUrl().toString()) ;
					if (bm==null)
						Log.e("RSS Thumb Loader", "FAILED");
					
					tedList.add( new tedRow(bm,
								 		rssItem.getTitle(),
								 		rssItem.getEnclosure().getUrl().toString()
								 		)
								);
					publishProgress();
					
					nit--; if (nit<=0) break;
				}
				
				
			

			} catch (RSSReaderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			return null;
		}


	}

	//String image_URL= "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/c62.62.776.776/s50x50/988669_10152882434160652_1893030052_n.png?oh=1bafe3a68d7a38c7f58f5a3935c09c74&oe=55DE006D&__gda__=1439727289_268cde1e06c9d45c4bd2b9089c5a7dac";

	public class MyCustomAdapter extends ArrayAdapter<tedRow> {
		///Bitmap bm;

		public MyCustomAdapter(Context context, int resource, ArrayList<tedRow> items) {
			super(context, resource, items);
		}		

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			//return super.getView(position, convertView, parent);

			View row = convertView;

			if(row==null){
				LayoutInflater inflater=getLayoutInflater();
				row=inflater.inflate(R.layout.row, parent, false);
			}

			TextView label=(TextView)row.findViewById(R.id.ted_title);
			label.setText(tedList.get(position).title );
			ImageView icon=(ImageView)row.findViewById(R.id.icon);

			icon.setImageBitmap(tedList.get(position).thumb);

			return row;
		}
	}

	Bitmap bm ;
	
	private void preLoadSrcBitmap(String image_URL){
	
		BitmapFactory.Options bmOptions;
		bmOptions = new BitmapFactory.Options();
		bmOptions.inSampleSize = 1;
		bm = LoadImage(image_URL, bmOptions);
	}



	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.main);

		/*setListAdapter(new ArrayAdapter<String>(this,
       R.layout.row, R.id.weekofday, DayOfWeek));*/
		tedList=null;
		tedList = new ArrayList<tedRow>();
		
		new backgroundLoadListView().execute();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		//super.onListItemClick(l, v, position, id);
		//String selection = l.getItemAtPosition(position).toString();
		//Toast.makeText(this, selection, Toast.LENGTH_LONG).show();
		String selection = tedList.get(position).videoUrl.toString();
		Toast.makeText(this, selection, Toast.LENGTH_LONG).show();
		
		String desc = tedList.get(position).title.toString();
		
		Intent myIntent = new Intent(this, FullscreenActivity.class);
		myIntent.putExtra("VIDEOURL", selection ); //Optional parameters
		myIntent.putExtra("VIDEODESC", desc ); //Optional parameters
		startActivity(myIntent);
		
	}

	private Bitmap LoadImage(String URL, BitmapFactory.Options options)
	{     
		Bitmap bitmap = null;
		InputStream in = null;     
		try {
			in = OpenHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			in.close();
		} catch (IOException e1) {
		}

		return bitmap;               
	}

	private InputStream OpenHttpConnection(String strURL) throws IOException{
		InputStream inputStream = null;
		URL url = new URL(strURL);
		URLConnection conn = url.openConnection();

		try{
			HttpURLConnection httpConn = (HttpURLConnection)conn;
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inputStream = httpConn.getInputStream(); 
			} 
		}
		catch (Exception ex){
		}

		return inputStream;
	}
}