package vv.zlobin.tedlistview.model;

import java.util.ArrayList;

public class Movie {
	private String title, thumbnailUrl;
	private String year;
	private String videoDesc;
	private String videoURL;
	private ArrayList<String> genre;

	public Movie() {
	}

	public Movie(String name, String thumbnailUrl, String year, String rating,
			ArrayList<String> genre, String vidUrl) {
		this.title = name;
		this.thumbnailUrl = thumbnailUrl;
		this.year = year;
		this.videoDesc = rating;
		this.genre = genre;
		this.setVideoURL(vidUrl);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String name) {
		this.title = name;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDescription() {
		return videoDesc;
	}

	public void setDescription(String str) {
		this.videoDesc = str;
	}

	public ArrayList<String> getGenre() {
		return genre;
	}

	public void setGenre(ArrayList<String> genre) {
		this.genre = genre;
	}

	public String getVideoURL() {
		return videoURL;
	}

	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

}