package vv.zlobin.ted;

import vv.zlobin.ted.R;
import vv.zlobin.ted.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 5000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
    //private static final int HIDER_FLAGS = SystemUiHider.FLAG_FULLSCREEN;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private VideoView vidView; 
    int position;
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      
      VideoView vidView = (VideoView)findViewById(R.id.myVideo);
      FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)vidView.getLayoutParams();
      if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
          layoutParams.width = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
          layoutParams.height = android.view.ViewGroup.LayoutParams.MATCH_PARENT;
      } else {
          layoutParams.width = android.view.ViewGroup.LayoutParams.MATCH_PARENT;
          layoutParams.height = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
      }
      vidView.setLayoutParams(layoutParams);
     
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.myVideo);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        
        /// UNCOMMENT IF YOU ADD ANY CUSTOM UI ELEMENTS
        ///findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
        findViewById(R.id.textDesc).setOnTouchListener(mDelayHideTouchListener);
        
        Intent intent = getIntent();
        String videoUrl = intent.getStringExtra("VIDEOURL"); 
        vidView = (VideoView)findViewById(R.id.myVideo);
        
        String videoDesc = intent.getStringExtra("VIDEODESC"); 
        TextView descView = (TextView) findViewById(R.id.textDesc);
        descView.setText(videoDesc);

        String videoTitle = intent.getStringExtra("VIDEOTITLE");
        setTitle(videoTitle);

        
        Uri vidUri = Uri.parse(videoUrl);
        vidView.setVideoURI(vidUri);
        position =0;
        //vidView.start();
        
        if (mediaControls == null) {
        	mediaControls = new MediaController(this);
        }
        vidView.setMediaController(mediaControls);
        
		vidView.requestFocus();
		//we also set an setOnPreparedListener in order to know when the video file is ready for playback
		vidView.setOnPreparedListener(new OnPreparedListener() {
		
			public void onPrepared(MediaPlayer mediaPlayer) {
				// close the progress bar and play the video
				///progressDialog.dismiss();
				
				//if we have a position on savedInstanceState, the video playback should start from here
				vidView.seekTo(position);
				if (position == 0) {
					vidView.start();
				} else {
					//if we come from a resumed activity, video playback will be paused
					vidView.pause();
				}
			}
		});
        
    }

    MediaController mediaControls;
    @Override
    protected void onResume() {
        super.onResume();
        vidView.seekTo(position);
    }
    @Override
    protected void onPause() {
        super.onPause();
        position = vidView.getCurrentPosition();
    }
    
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
          super.onSaveInstanceState(savedInstanceState);
          savedInstanceState.putInt("Position", vidView.getCurrentPosition());
          vidView.pause();
    }    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
          super.onRestoreInstanceState(savedInstanceState);
          position = savedInstanceState.getInt("Position");
          vidView.seekTo(position);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(200);
    }


    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
