package vv.zlobin.ted;

import vv.zlobin.tedlistview.adapter.CustomListAdapter;
import vv.zlobin.tedlistview.app.AppController;
import vv.zlobin.tedlistview.model.Movie;
 
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
 

import org.mcsoxford.rss.RSSConfig;
import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSParser;
import org.mcsoxford.rss.RSSParserSPI;
import org.mcsoxford.rss.RSSReader;
import org.mcsoxford.rss.RSSReaderException;
 
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
 
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
 
public class MainActivity extends Activity {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();
 
    // Movies json url
    private static final String url = "http://www.ted.com/themes/rss/id/6";
    private ProgressDialog pDialog;
    private List<Movie> movieList = new ArrayList<Movie>();
    private ListView listView;
    private CustomListAdapter adapter;
 
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
 
        listView = (ListView) findViewById(R.id.list);
        adapter = new CustomListAdapter(this, movieList);
        listView.setAdapter(adapter);
 
        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();
 
        // changing action bar color
        
        //getActionBar().setBackgroundDrawable(
                //new ColorDrawable(Color.parseColor("#1b1b1b")));
 
        // Creating volley request obj
        StringRequest movieReq = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d(TAG, response.toString());

                        try {
                        	InputStream stream = new ByteArrayInputStream(response.getBytes());
                        	RSSParser parser = new RSSParser(new RSSConfig());
                        	RSSFeed feed = parser.parse(stream);
                        	
                        	List<RSSItem> rssItems = feed.getItems();                        

                        	for(RSSItem rssItem : rssItems) {
                        		
                        		Movie movie = new Movie();
                        		movie.setTitle(rssItem.getTitle());
                        		movie.setThumbnailUrl(rssItem.getThumbnails().get(0).getUrl().toString());
                        		movie.setDescription(rssItem.getDescription());
                        		
                        		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        		movie.setYear(sdf.format(rssItem.getPubDate()));//!!! TODO
                        		
                        		movie.setVideoURL(rssItem.getEnclosure().getUrl().toString());
                        		
                        		// adding movie to movies array
                        		movieList.add(movie);

                        	}
                        } catch (Exception e) {
                        	// TODO Auto-generated catch block
                        	e.printStackTrace();
                        	Log.e(TAG,e.getMessage() != null ? e.getMessage() : "error");
                        	
                        }
                        
                        hidePDialog();
                        
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();
 
                    }
                });
 
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(movieReq);
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){ 

        	@Override
        	public void onItemClick(AdapterView<?> parent, View view,
        			int position, long id) {

        		String selection, desc, title;
        		desc = movieList.get(position).getDescription();
        		selection = movieList.get(position).getVideoURL();
        		title= movieList.get(position).getTitle();
        		
        		Intent myIntent = new Intent(getApplicationContext(), FullscreenActivity.class);
        		myIntent.putExtra("VIDEOURL", selection ); //Optional parameters
        		myIntent.putExtra("VIDEOTITLE", title ); //Optional parameters
        		myIntent.putExtra("VIDEODESC", desc ); //Optional parameters
        		startActivity(myIntent);
        		
        	}
        });        
    }
 
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
 
    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        // �������� ��� ���������� ������ ����
        switch (item.getItemId()) 
    	{
        case R.id.action_settings:
            ShowAbout();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void ShowAbout() {
		Intent myIntent = new Intent(this, AboutActivity.class);
		startActivity(myIntent);
	}   
 
}



/*
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;



import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSReader;
import org.mcsoxford.rss.RSSReaderException;
import org.xml.sax.SAXException;
import vv.zlobin.ted.R;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	String[] month = {
			"January", "February", "March", "April",
			"May", "June", "July", "August",
			"September", "October", "November", "December"
	};
	class tedRow {
		String title;
		Bitmap thumb;
		String videoUrl;
		tedRow(Bitmap preLoadSrcBitmap, String inTitle,
				String videoLink) { 
			thumb = preLoadSrcBitmap; 
			title = inTitle;
			videoUrl = videoLink;
		}
	}
	ArrayList<tedRow> tedList;
	ArrayAdapter adapter; 
	
	public class backgroundLoadListView extends
	AsyncTask<Void, Void, Void> {

		@Override
		protected void onPostExecute(Void result) {
			//We moved it to PreEx: setListAdapter(new MyCustomAdapter(MainActivity.this, R.layout.row, tedList));
			Toast.makeText(MainActivity.this,
					"onPostExecute \n: setListAdapter after bitmap preloaded",
					Toast.LENGTH_LONG).show();
			setListAdapter(new MyCustomAdapter(MainActivity.this, R.layout.row, tedList));
			((ArrayAdapter<tedRow>) getListAdapter()).notifyDataSetChanged();
		}

		@Override
		protected void onPreExecute() {
			Toast.makeText(MainActivity.this,
					"onPreExecute \n: preload bitmap in AsyncTask",
					Toast.LENGTH_LONG).show();
			tedList.clear();
		}
		
		protected void onProgressUpdate(Integer... progress) {

		}

		@Override
		protected Void doInBackground(Void... params) {
		
			try {
				//url = new URL("http://www.ted.com/themes/rss/id/6");
				
				RSSReader reader = new RSSReader(); 
				String uri = "http://www.ted.com/themes/rss/id/6"; 
				RSSFeed feed = reader.load(uri);

				List<RSSItem> rssItems = feed.getItems();
				int maxItems=20;
				int nit=maxItems;
				
				bm=BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);;
				
				for(RSSItem rssItem : rssItems) {
					Log.i("RSS Reader", rssItem.getTitle());
					//Log.i("RSS Reader", rssItem.getLink().toString());
					Log.i("RSS Reader", rssItem.getEnclosure().getUrl().toString() );
					Log.i("RSS Reader", rssItem.getThumbnails().get(0).getUrl().toString());
					
					//preLoadSrcBitmap(rssItem.getThumbnails().get(0).getUrl().toString()) ;
					if (bm==null)
						Log.e("RSS Thumb Loader", "FAILED");
					
					tedList.add( new tedRow(bm,
								 		rssItem.getTitle(),
								 		rssItem.getEnclosure().getUrl().toString()
								 		)
								);
					publishProgress();
					
					nit--; if (nit<=0) break;
				}
				
				
			

			} catch (RSSReaderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			return null;
		}


	}

	//String image_URL= "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/c62.62.776.776/s50x50/988669_10152882434160652_1893030052_n.png?oh=1bafe3a68d7a38c7f58f5a3935c09c74&oe=55DE006D&__gda__=1439727289_268cde1e06c9d45c4bd2b9089c5a7dac";

	public class MyCustomAdapter extends ArrayAdapter<tedRow> {
		///Bitmap bm;

		public MyCustomAdapter(Context context, int resource, ArrayList<tedRow> items) {
			super(context, resource, items);
		}		

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			//return super.getView(position, convertView, parent);

			View row = convertView;

			if(row==null){
				LayoutInflater inflater=getLayoutInflater();
				row=inflater.inflate(R.layout.row, parent, false);
			}

			TextView label=(TextView)row.findViewById(R.id.ted_title);
			label.setText(tedList.get(position).title );
			ImageView icon=(ImageView)row.findViewById(R.id.icon);

			icon.setImageBitmap(tedList.get(position).thumb);

			return row;
		}
	}

	Bitmap bm ;
	
	private void preLoadSrcBitmap(String image_URL){
	
		BitmapFactory.Options bmOptions;
		bmOptions = new BitmapFactory.Options();
		bmOptions.inSampleSize = 1;
		bm = LoadImage(image_URL, bmOptions);
	}



	//** Called when the activity is first created. *
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.main);

		///*setListAdapter(new ArrayAdapter<String>(this,
       //R.layout.row, R.id.weekofday, DayOfWeek));* /
		tedList=null;
		tedList = new ArrayList<tedRow>();
		
		new backgroundLoadListView().execute();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		//super.onListItemClick(l, v, position, id);
		//String selection = l.getItemAtPosition(position).toString();
		//Toast.makeText(this, selection, Toast.LENGTH_LONG).show();
		String selection = tedList.get(position).videoUrl.toString();
		Toast.makeText(this, selection, Toast.LENGTH_LONG).show();
		
		String desc = tedList.get(position).title.toString();
		
		Intent myIntent = new Intent(this, FullscreenActivity.class);
		myIntent.putExtra("VIDEOURL", selection ); //Optional parameters
		myIntent.putExtra("VIDEODESC", desc ); //Optional parameters
		startActivity(myIntent);
		
	}

	private Bitmap LoadImage(String URL, BitmapFactory.Options options)
	{     
		Bitmap bitmap = null;
		InputStream in = null;     
		try {
			in = OpenHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			in.close();
		} catch (IOException e1) {
		}

		return bitmap;               
	}

	private InputStream OpenHttpConnection(String strURL) throws IOException{
		InputStream inputStream = null;
		URL url = new URL(strURL);
		URLConnection conn = url.openConnection();

		try{
			HttpURLConnection httpConn = (HttpURLConnection)conn;
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inputStream = httpConn.getInputStream(); 
			} 
		}
		catch (Exception ex){
		}

		return inputStream;
	}
}

*/